﻿// Copyright ©2019, Andre Plourde
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without 
// restriction, including without limitation the rights to use, copy, modify, merge, publish, 
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom 
// the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or 
// substantial portions of the Software.
//
// The Software is provided “as is”, without warranty of any kind, express or implied, 
// including but not limited to the warranties of merchantability, fitness for a particular 
// purpose and noninfringement. In no event shall the authors or copyright holders X be 
// liable for any claim, damages or other liability, whether in an action of contract, tort or
// otherwise, arising from, out of or in connection with the software or the use or other 
// dealings in the Software.

using System.Collections.Generic;

using UnityEngine;

/// <summary>
/// c'est le systeme qui gere tout le fenetrage, c'est un singleton pour reduire les finds
/// </summary>
public class WindowSystem : MonoBehaviour {

    #region singleton datas
    static WindowSystem _instance = null;
    public static WindowSystem Instance { get { return _instance; } set { _instance = value; } }
    #endregion

    #region serializeField
    [SerializeField, Tooltip("le prefab a utiliser pour une fenetre")]
    private Window windowPrefab = null;

    [SerializeField, Tooltip("le transform couvrant la zone des fenetre permetant d'afficher des information en mode modal")]
    private Transform ModaleZone = null;

    [SerializeField, Tooltip("Le parent ou seront instanciés les fenetres")]
    private Transform WindowsZone = null;

    [SerializeField, Tooltip("la liste des prefab des contenue en fonction du nom d'une fenetre")]
    List<ContentPrefabPair> contentPrefabs = new List<ContentPrefabPair>();
    Dictionary<string, WindowContentPrefab> _contentPrefabs = new Dictionary<string, WindowContentPrefab>();

    /// <summary>
    /// ceci est temporaire en attendant de faire un custom editor du dictionary _contentPrefabs
    /// </summary>
    [System.Serializable]
    private class ContentPrefabPair {
        public string name;
        public WindowContentPrefab prefab;
    }
    #endregion

    private void Awake() {
        //application du principe de singleton
        if(_instance != null) {
            Destroy(this.gameObject);
        }
        _instance = this;

        //sera change avec le custom editor
        foreach(ContentPrefabPair p in contentPrefabs) {
            _contentPrefabs.Add(p.name, p.prefab);
        }
    }

    /// <summary> permet d'afficher un message en mode nodal </summary>
    /// <param name="message">le message a afficher</param>
    public void ShowMessage(string message) {
        ModaleZone.gameObject.SetActive(true);

        WindowMessage messageView = ModaleZone.GetComponentInChildren<WindowMessage>();
        messageView.SetMessage(message);
        messageView.OnClose += OnMessageClose;
    }

    /// <summary> permet de lancer la creation d'une fenetre depuis le GUI </summary>
    /// <param name="name"></param>
    public void GUINewWindow(string name) {
        Window w = NewWindow(name);
        if (null != w)
            w.Show();
    }

    /// <summary>
    /// instancier une nouvelle fenetre dans la zone
    /// </summary>
    /// <param name="name">le type de contenue de l'ecran</param>
    /// <returns>le component Window de la fenetre ou null si introuvable</returns>
    public Window NewWindow(string name) {
        if (!_contentPrefabs.ContainsKey(name))
            return null;

        //preparer le type de fenetre demande
        WindowContentPrefab data = Instantiate<WindowContentPrefab>(_contentPrefabs[name]);
        
        //preparer la fenetre
        Window newWindow = Instantiate<Window>(windowPrefab);
        newWindow.name = name;
        newWindow.Hide();
        newWindow.transform.SetParent(WindowsZone);
        newWindow.SetPosition(Vector2.zero);
        data.transform.SetParent(newWindow.Content);
        RectTransform r = data.GetComponent<RectTransform>();
        r.anchorMin = Vector2.zero;
        r.anchorMax = Vector2.one;
        r.offsetMin = Vector2.zero;
        r.offsetMax = Vector2.zero;

        data.SetWindow(newWindow);

        newWindow.WindowType = name;

        ClampWindow(newWindow);

        return newWindow;
    }

    /// <summary> rammener une fenetre au 1er plan </summary>
    /// <param name="window">la fenetre a ramener</param>
    public void BringToFront(Window window) {
        window.transform.SetAsLastSibling();
    }

    /// <summary> Un evenement levé quand on ferme le message modal affiche </summary>
    private void OnMessageClose() {
        WindowMessage messageView = ModaleZone.GetComponentInChildren<WindowMessage>();
        messageView.OnClose -= OnMessageClose;
        ModaleZone.gameObject.SetActive(false);
    }

    /// <summary> permet de forcer qu'une fenetre ne sorte pas de la zone de gestion du systeme </summary>
    /// <param name="w">la fenetre a clamper</param>
    public void ClampWindow(Window w) {
        if (null == w)
            return;

        Rect rect = GetComponent<RectTransform>().rect;
        Rect myRect = w.GetComponent<RectTransform>().rect;

        //clamp bottom
        float minY = -rect.height * 0.5f;
        if (w.transform.localPosition.y - myRect.height < minY) {
            w.transform.localPosition = new Vector2(w.transform.localPosition.x, myRect.height + minY);
        }

        //clamp Left
        if (w.transform.localPosition.x  < -rect.width * 0.5f) {
            w.transform.localPosition = new Vector2(-rect.width * 0.5f, w.transform.localPosition.y);
        }

        //clamp right
        if (w.transform.localPosition.x + myRect.width > rect.width * 0.5f) {
            w.transform.localPosition = new Vector2(rect.width * 0.5f - myRect.width, w.transform.localPosition.y);
        }

        //clamp top
        if (w.transform.localPosition.y > rect.height * 0.5f) {
            w.transform.localPosition = new Vector2(w.transform.localPosition.x, rect.height * 0.5f);
        }

    }
}