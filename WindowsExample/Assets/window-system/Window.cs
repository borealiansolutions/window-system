﻿// Copyright ©2019, Andre Plourde
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without 
// restriction, including without limitation the rights to use, copy, modify, merge, publish, 
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom 
// the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or 
// substantial portions of the Software.
//
// The Software is provided “as is”, without warranty of any kind, express or implied, 
// including but not limited to the warranties of merchantability, fitness for a particular 
// purpose and noninfringement. In no event shall the authors or copyright holders X be 
// liable for any claim, damages or other liability, whether in an action of contract, tort or
// otherwise, arising from, out of or in connection with the software or the use or other 
// dealings in the Software.


using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Le component qui est ajouté sur une fenetre, c'est la base du fonctionnement de la fenetre
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class Window : MonoBehaviour, IPointerClickHandler{

    #region events

    public delegate void WindowAction(Window w);

    /// <summary> un evenement qui est levé quand le fenetr est fermee </summary>
    public event WindowAction OnClose = delegate { };

    #endregion events

    #region serializeField
    [SerializeField, Tooltip("Bouton a utiliser pour fermer la fenetre")]
    private Button closeButton = null;

    [SerializeField, Tooltip("La zone qui contiendra les données")]
    private Transform content = null;

    [SerializeField, Tooltip("Le titre de la fenetre")]
    private TMPro.TextMeshProUGUI title = null;

    [SerializeField, Tooltip("L'objet a activer pour indiquer que la fenetre est en chargement")]
    private CanvasGroup loading = null;

    #endregion

    /// <summary> la largeur minimale de la fenetre </summary>
    public int MinWidth { get; set; }
    /// <summary> La hauteur minimale de la fenetre </summary>
    public int MinHeight { get; set; }

    private RectTransform mRectTrans = null;

    /// <summary> le WindowSystem en charge de cette fenetre </summary>
    WindowSystem WindowSystem { get; set; }

    /// <summary> recupérere le transform du contenue de cette fenetre </summary>
    public Transform Content {
        get { return content; }
    }

    /// <summary> Permet de connaitre le type de fenetre </summary>
    public string WindowType { get; set; }

    private void Awake() {
        SetLoading(false);

        if (null != closeButton) {
            closeButton.onClick.AddListener(CloseWindow);
        }
        mRectTrans = GetComponent<RectTransform>();
        mRectTrans.anchorMin = new Vector2(0.0f, 1.0f);
        mRectTrans.anchorMax = new Vector2(0.0f, 1.0f);
    }
    
    /// <summary>  cacher la fenetre </summary>
    public void Hide() {
        CanvasGroup c = GetComponent<CanvasGroup>();
        c.alpha = 0.0f;
        c.interactable = false;
        c.blocksRaycasts = false;
    }

    /// <summary> afficher la fenetre  </summary>
    public void Show() {
        CanvasGroup c = GetComponent<CanvasGroup>();
        c.alpha = 1.0f;
        c.interactable = true;
        c.blocksRaycasts = true;
    }

    /// <summary> mettre a jour la position de la fenetre par rapport a la zone dans laquelle elle evolue </summary>
    /// <param name="position"></param>
    public void SetPosition(Vector2 position) {
        mRectTrans.anchoredPosition = position;
    }

    /// <summary>
    /// le titre de la fenetre
    /// </summary>
    public string Title {
        get { return (title == null ? "" : title.text); }
        set {
            if (title != null) {
                title.text = value;
            }
        }
    }

    /// <summary>
    /// demander la fermeture de cette fenetre
    /// </summary>
    private void CloseWindow() {
        OnClose(this);
        GameObject.Destroy(gameObject);
    }

    public void OnPointerClick(PointerEventData data) {
        if (null != WindowSystem)
            WindowSystem.BringToFront(this);
    }

    /// <summary> changer la largeur de la fenetre </summary>
    /// <param name="width"></param>
    public void SetWidth(int width) {
        if (width < MinWidth)
            return;

        mRectTrans.sizeDelta = new Vector2(width, mRectTrans.sizeDelta.y);
    }

    public void SetHeight(int height) {
        if (height < MinHeight)
            return;

        mRectTrans.sizeDelta = new Vector2(mRectTrans.sizeDelta.x,height);
    }

    /// <summary>
    /// demander a cette fenetre de ne pas deborder de la zone de gestion
    /// </summary>
    public void Clamp() {
        WindowSystem.ClampWindow(this);
    }

    /// <summary>
    /// passer cette fenetre en mode "chargement"
    /// </summary>
    /// <param name="isLoading">true pour indiquer en cours de chargement</param>
    public void SetLoading(bool isLoading) {
        if(null != loading) {
            if (isLoading) {
                loading.gameObject.SetActive(true);
                loading.alpha = 1.0f;
                loading.interactable = true;
                loading.blocksRaycasts = true;
            } else {
                loading.gameObject.SetActive(false);
                loading.alpha = 0.0f;
                loading.interactable = false;
                loading.blocksRaycasts = false;
            }
        }
    }
}